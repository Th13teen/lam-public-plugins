--- Godmode Plugin

local PLUGIN = {}
PLUGIN.Commands = {}
PLUGIN.Hooks = {}

PLUGIN.Name = "Godmode"
PLUGIN.Author = "D182 & Th13teen"
PLUGIN.Version = "1" --Optional (Assumed 1 if not present)
PLUGIN.RequiredLAM = "1" --Optional (Assumed the same as LAM if not present )

PLUGIN.GodmodePlayers = {}

local godmode_on_spawn
local is_sandbox = false
if GetConVarString( "gamemode" ) == "sandbox" then
	is_sandbox = true
	godmode_on_spawn = CreateConVar( "lam_godmode_on_spawn", 1, { FCVAR_REPLICATED, FCVAR_ARCHIVE, FCVAR_NOTIFY } )
else
	godmode_on_spawn = CreateConVar( "lam_godmode_on_spawn", 0, { FCVAR_REPLICATED, FCVAR_ARCHIVE, FCVAR_NOTIFY } )
end

--- Handle Godmode
-- @function handleGodmode
-- @param callingPly Calling Player
-- @param args Arguments
local function handleGodmode( callingPly, args )
	local target = LAM.FindPlayerByName( args[1] ) or callingPly

	if not callingPly:IsAdmin() then
		target = callingPly
	end

	if target then
		if not LAM.IsImmune( target, callingPly ) then
			local toggle = !PLUGIN.GodmodePlayers[ target ]
			PLUGIN.GodmodePlayers[ target ] = toggle
			if ( toggle ) then
				LAM.FormatChatPrint( callingPly, "has enabled godmode for", target )
			else
				LAM.FormatChatPrint( callingPly, "has disabled godmode for", target )
			end
		else
			return "You can't target this person!"
		end
	else
		return "Couldn't find a Player with that name!"
	end
end

PLUGIN.Hooks.EntityTakeDamage = function( ply, dmg )
	if ( PLUGIN.GodmodePlayers[ ply ] ) then
		dmg:ScaleDamage( 0 )
	end
	return dmg
end

PLUGIN.Hooks.PlayerSpawn = function( ply, sid, uid )
	PLUGIN.GodmodePlayers[ ply ] = godmode_on_spawn:GetBool()
end

if ( is_sandbox ) then
	PLUGIN.Commands.God = { func = handleGodmode, perms = "sandbox.godmode", args = { } }
else
	PLUGIN.Commands.God = { func = handleGodmode, perms = "lam.management.god", args = { } } -- We are not in sandbox, make /god admin only
end			

LAM.RegisterPlugin( PLUGIN )