--- Nametag Plugin

local PLUGIN = {}
PLUGIN.Commands = {}
PLUGIN.Hooks = {}

PLUGIN.Name = "Nametags"
PLUGIN.Author = "D182"
PLUGIN.Version = "1" --Optional (Assumed 1 if not present)
PLUGIN.RequiredLAM = "1" --Optional (Assumed the same as LAM if not present )

local function handleTitle( callingPly, args )
	if ( table.Count( args ) > 0 or args[1] == "\"\"" ) then
		local str = table.concat( args, " " )
		callingPly:SetNetworkedVar( "LAM Title", str )
		LAM.SaveProfileData( callingPly, PLUGIN.Name, { title = str } )
	else
		return "Your current Title: " .. callingPly:GetNetworkedVar( "LAM Title", "none" )
	end
end
PLUGIN.Commands.Title = { func = handleTitle, perms = "lam.core.title", args = { } }

local function handleClearTitle( callingPly, args )
	callingPly:SetNetworkedVar( "LAM Title", "" )
	LAM.SaveProfileData( callingPly, PLUGIN.Name, { title = "" } )
end
PLUGIN.Commands.ClearTitle = { func = handleClearTitle, perms = "lam.core.title", args = { } }

PLUGIN.Hooks[ "LAM PlayerReady" ] = function( ply )
	local data = LAM.LoadProfileData( ply, PLUGIN.Name )
	if ( type( data.title ) == "string" ) then
		local str = data.title
		print( "Player "..ply:Name().." with title "..str )
		ply:SetNetworkedVar( "LAM Title", str )
	end
end

LAM.RegisterPlugin( PLUGIN )