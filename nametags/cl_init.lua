--- Nametag Plugin

local PLUGIN = {}
PLUGIN.Commands = {}
PLUGIN.Hooks = {}

PLUGIN.Name = "Nametags"
PLUGIN.Author = "D182"
PLUGIN.Version = "1" --Optional (Assumed 1 if not present)
PLUGIN.RequiredLAM = "1" --Optional (Assumed the same as LAM if not present )

surface.CreateFont( "nametags_name", {
    font = "Coolvetica",
    size = 30,
    weight = 1000,
    outline = false
} )

surface.CreateFont( "nametags_title", {
    font = "Coolvetica",
    size = 19,
    weight = 1000,
    outline = false
} )

surface.CreateFont( "nametags_name_blur", {
    font = "Coolvetica",
    size = 30,
    weight = 1200,
    outline = false,
    blursize = 5,
    blur = 10
} )

surface.CreateFont( "nametags_title_blur", {
    font = "Coolvetica",
    size = 19,
    weight = 1000,
    outline = false,
    blursize = 5,
    blur = 10
} )

PLUGIN.Hooks.HUDPaint = function() 
	for k,v in pairs(player.GetAll()) do
		local phead = v:LookupBone("ValveBiped.Bip01_Head1") 
		local wpos

		if not phead then 
			wpos = v:GetPos() + Vector( 0, 0, 90 )
		else
			wpos = v:GetBonePosition(phead) + Vector( 0, 0, 30 ) 
		end
				
		local spos = wpos:ToScreen()

		local title = v:GetNetworkedVar( "LAM Title", "" )
		local tx = 20

		surface.SetFont("nametags_name")
		local len = surface.GetTextSize( v:Nick() )
		local tlen, tlen2 = surface.GetTextSize( title ), 0

		if string.len( title ) > 50 then
			local t = title
			title = { string.sub( t, 1, 50), string.sub( t, 51, string.len( t ) + 51) }
			tx = 40
			surface.SetFont("nametags_title")
			tlen = surface.GetTextSize( title[1] )
			tlen2 = surface.GetTextSize( title[2] )
		end
			
		local x, y = spos.x, spos.y 

		if v:GetPos():Distance( LocalPlayer():GetPos() ) > 250  or v == LocalPlayer() then continue end 

		draw.SimpleText( v:Nick(), "nametags_name_blur", x, y +5, Color( 5, 5, 5 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		draw.SimpleText( v:Nick(), "nametags_name", x, y + 5, LAM.GetRankColor( v ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)

		if tlen > 0 then

			if tx == 40 then
				draw.SimpleText( title[1], "nametags_title_blur", x , y + 27, Color( 5, 5, 5 ), TEXT_ALIGN_CENTER)
				draw.SimpleText( title[1], "nametags_title", x , y + 27, Color( 235, 235, 235 ), TEXT_ALIGN_CENTER)

				draw.SimpleText( title[2], "nametags_title_blur", x , y + 27, Color( 5, 5, 5 ), TEXT_ALIGN_CENTER)
				draw.SimpleText( title[2], "nametags_title", x, y + 47, Color( 235, 235, 235 ), TEXT_ALIGN_CENTER)
			else
				draw.SimpleText( title, "nametags_title_blur", x, y + 27, Color( 5, 5, 5 ) ,TEXT_ALIGN_CENTER)	
				draw.SimpleText( title, "nametags_title", x, y + 27, Color( 235, 235, 235 ) ,TEXT_ALIGN_CENTER)	
			end		
		end	
	end
end

LAM.RegisterPlugin( PLUGIN )